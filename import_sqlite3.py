import sqlite3

conn = sqlite3.connect('tarea1.db')


c = conn.cursor()


def menu1():
    tabla = [""" CREATE TABLE asig1(
        ID INTEGER PRIMARY KEY AUTOINCREMENT, 
        PALABRA TEXT NOT NULL,
        SIGNIFICADO TEXT NOT NULL
        );
        """
    ]      
conn.commit() 

def Principal():
    menu1()
    
    MENU === """
a) Agregar Palabra.
b) Editar Palabr existente.
c) Eliminar  Palabra existente.
d) Ver  Palabras.
e) Buscar un significado  Palabra.
f) exit.
Elige: """
    elec = ""
    while elec != "f":
        elec = input(MENU)

        if elec == "a":
            PALABRA = input("Ingrese una  palabra: ")
            old_significado = buscar_significado_palabra(PALABRA)
            if old_significado:
                print(f"La palabra '{PALABRA}' ya existe.")
            else:
                SIGNIFICADO = input("Ingresa el significado: ")
                agregarp(PALABRA, SIGNIFICADO)
                print("Palabr Agregada satisfactoriamente!")

        if elec == "b":
            PALABRA = input("Ingrese una  palabra para editar: ")
            newsignificado = input("Ingrese el nuevo significado: ")
            editarp(PALABRA, newsignificado)
            print("Palabra ingresada")

        if elec == "c":
            PALABRA = input("Ingrese una palabra para  eliminar: ")
            eliminarp(PALABRA)

        if elec == "d":
            palabras = obtenerp()
            print("=== Lista de Palabras ===")
            for PALABRA in palabras:
                print(PALABRA[0])

        if elec == "e":
            PALABRA = input(
                "Ingresa la Palabra para ver su  significado: ")
            SIGNIFICADO = buscar_significado_palabra1(PALABRA)
            if SIGNIFICADO:
                print(f"El significado de '{PALABRA}' es:\n{SIGNIFICADO[0]}")
            else:
                print(f"La Palabra '{PALABRA}' no fue encontrada")
    
def obtenerp():
    c = conn.cursor()
    consult = "SELECT PALABRA FROM asig1"
    c.execute(consult)
    return c.fetchall()
                
def editarp(PALABRA, nuevosign):
    c = conn.cursor()
    sent = "UPDATE diccionario SET SIGNIFICADO = ? WHERE PALABRA = ?"
    c.execute(sent, [nuevosign, PALABRA])
    conn.commit()

def eliminarp(PALABRA):
    c = conn.cursor()
    sent = "DELETE FROM asig1 WHERE PALABRA = ?"
    c.execute(sent, [PALABRA])
    conn.commit()

def agregarp(PALABRA, SIGNIFICADO):
    c = conn.cursor()
    sent = "INSERT INTO asig1 (PALABRA, SIGNIFICADO) VALUES (?, ?)"
    c.execute(sent, [PALABRA, SIGNIFICADO])
    conn.commit()

def buscar_significado_palabra(PALABRA):
    c = conn.cursor()
    consult = "SELECT SIGNIFICADO FROM asig1 WHERE PALABRA = ?"
    c.execute(consult, [PALABRA])
    return c.fetchone()

if __name__ == '__main__':
    Principal()
  

 
conn.close()
